package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Album;

@Service
public class AlbumService {

    // cách 1 , tạo 1 object và thêm vào 3 thuộc tính 
    Album DanTruong = new Album(1, "vol1" , new ArrayList<String>(){
        {
            add(new String("phong ba tình đời"));
            add(new String("buồn như đá thời gian"));
        }
    });

    // cách 2 tạo 1 object và thêm 2 thuộc tính , có thể gọi riêng biệt , từng country , từng region 
    Album PhuongThanh = new Album(2, "vol2" , new ArrayList<String>(){
        {
            add(new String("tình xa khuất"));
            add(new String("trống vắng"));
        }
    });
    

    public ArrayList<Album> getAlbumDanTruong(){
        ArrayList<Album> DanTruongVol1 = new ArrayList<Album>();

        DanTruongVol1.add(DanTruong);

        return DanTruongVol1;
    }

    public ArrayList<Album> getAlbumPhuongThanh(){
        ArrayList<Album> PhuongThanhVol1 = new ArrayList<Album>();

        PhuongThanhVol1.add(PhuongThanh);

        return PhuongThanhVol1;
    }
    

    public ArrayList<Album> getAllAlbum(){
        ArrayList<Album> AllAlbum = new ArrayList<Album>();

        AllAlbum.add(DanTruong);
        AllAlbum.add(PhuongThanh);

        return AllAlbum;
    }
}
